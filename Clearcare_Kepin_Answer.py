class BaseClass(object):
  import inspect
  def is_the_one(self):
    import inspect
    return inspect.isclass(self)
This is first commit message and for testing.
class Solutions(object):

    def greater_than_avg(self, nums):
        return [item for item in nums if item > sum(nums)/len(nums)]

    def sort_fruit(self, fruit):
      return sorted(fruit, key=lambda key:key['count'])
        

    def reverse_dict(self, d):
      newd={}
      for key,value in d.items():
        newd.update({value:key})
      return newd

    def week_start_end(self, dt):
      import datetime
      start_of = dt - datetime.timedelta(days = dt.weekday())
      end_of = start_of + datetime.timedelta(days = 6)
      start_of  = start_of.replace(minute=0, hour=0, second=0, microsecond=0)
      end_of  = end_of.replace(minute=59, hour=23, second=59, microsecond=999999)
      return (start_of, end_of)

    def month_last_day(self, dt):
      import calendar 
      return calendar.monthrange(dt.year,dt.month)[1]

    def string_parse(self, text):
      import re
      likes,dislikes = [],[]
      pairs = re.split("\+-*\+-*\+\n?",text)[2:-1] #Drop the header and the tail
      for p in pairs:
        like,dislike = [],[]
        for l in p.split('\n'):
          pair = l.split('|')
          if len(pair) > 1:
            # Not a blank line
            like.append(unicode(pair[1].strip()))
            dislike.append(unicode(pair[2].strip()))
        if len(like) > 0:
          likes.append(" ".join(like))
        if len(dislike) > 0:
          dislikes.append(" ".join(dislike))
      newlike=[]
      for item in likes:
        newlike.append(item.strip())
        newlike1=[]
      for item in dislikes:
        newlike1.append(item.strip())
      aa = zip(newlike, newlike1)
      aq=[]
      for item in aa:
        aq.append(item)
      return aq


    def palindrome_test_function(self):
      def test_function(s):
        s = s.replace(",","")
        s = s.replace(":","")
        s = s.replace(".","")
        s = s.replace(" ","")
        reve = s[::-1]
        if s.lower() == reve.lower():
          return True
        return False
      return test_function

    def get_dynamic_classes(self, names):
      class Base(object):
        def is_the_one(self):
          if self.__class__.__name__ =="Neo":
            return True
          else:
            return False
      l=[]
      for name in names:
        l.append(type(name, (Base,),{'__doc__':'class created by type'}))
      return l