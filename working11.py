text = """
This is for testing purpose and it should be working and this is not the way it should be working.
+------------------------------------+-----------------------------------+
| likes                              | dislikes                          |
+------------------------------------+-----------------------------------+
| Meritocracy                        | Favoritism, ass-kissing, politics |
+------------------------------------+-----------------------------------+
| Healthy debates and collaboration  | Ego-driven rhetoric, drama and FUD|
|                                    | to get one's way                  |
+------------------------------------+-----------------------------------+
| Autonomy given by confident leaders| Micro-management by insecure      |
| capable of attracting top-tier     | managers compensating for a weak, |
| talent                             | immature team                     |
+------------------------------------+-----------------------------------+
| Fluid communication, brief, ad-hoc | Formal meetings, endless debate   |
| discussions, white-boarding, and   |                                   |
| quick but informed decisions       |                                   |
+------------------------------------+-----------------------------------+
| Where else can I help out?         | I'm blocked by..., I only know how|
|                                    | to...                             |
+------------------------------------+-----------------------------------+
"""
def string_parse(text):
  import re
  likes,dislikes = [],[]
  pairs = re.split("\+-*\+-*\+\n?",text)[2:-1] #Drop the header and the tail
  for p in pairs:
    like,dislike = [],[]
    for l in p.split('\n'):
      pair = l.split('|')
      if len(pair) > 1:
        # Not a blank line
        like.append(" ".join(unicode(pair[1].split())))
        dislike.append(" ".join(unicode(pair[2].split())))
    if len(like) > 0:
      likes.append(" ".join(like))
    if len(dislike) > 0:
      dislikes.append(" ".join(dislike))
  newlike=[]
  print(likes)
  for item in likes:
  	newlike.append(item.strip())
  	newlike1=[]
  for item in dislikes:
  	newlike1.append(item.strip())
  aa = zip(newlike, newlike1)
  aq=[]
  for item in aa:
    aq.append(item)
  return aq
print(string_parse(text))